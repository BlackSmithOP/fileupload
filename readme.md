# FileUpload 📁

<img alt="Website" src="https://img.shields.io/website?down_message=Down&label=Website&up_message=Up&url=https%3A%2F%2Fblacksmithop.bitbucket.io%2F">


<a target="_blank" href="https://blacksmithop.bitbucket.io/">Link</a>

Track progress at [Trello](https://trello.com/b/ahhFKmLq/fileupload)


### Features 
- [x] Responsive header
- [x] Responsive footer
- [ ] Dark mode toggle
- [x] File list 
- [ ] Responsive content
- [x] Toasts
- [ ] Stacking toasts
- [ ] Responsive search bar
- [ ] Toggle hints
### Cloning
<img src="https://i.ibb.co/WvrHxCn/How-to-clone.gif" alt="drawing" width=500"/>
