
document.addEventListener("DOMContentLoaded", function (event) {

    var toastTrigger = document.getElementById('liveToastBtn')
    var toastLiveExample = document.getElementById('liveToast')

    toastTrigger.addEventListener('click', function () {

        var toast = new bootstrap.Toast(toastLiveExample)

        toast.show()
    })

    $(".darkmode").click(function () {
        $("body").toggleClass("dark")
            .css(
                $("body").hasClass("dark") ?
                    {
                        background: "#202225", color: "#f9f9f9"
                    }
                    :
                    {
                        background: "#f9f9f9", color: "#202225"
                    }
            );

        $(".nav-link").toggleClass("text-white");
        $(".fa-circle-chevron-down").toggleClass("text-white");
    });


});